#!/bin/env bash

DTSTAMP=$(date +%F_%H-%M-%S.%N)
OUTDIR=$(pwd)/pngseq_${DTSTAMP}
INFILE=$1

set -e

function conv {
	rm -rf ${OUTDIR}
	mkdir ${OUTDIR}
	ffmpeg -y -i ${INFILE} -c:v png ${OUTDIR}/seq_%04d.png
}

time conv
