//
// Created by gnare on 1/31/24.
//

#ifndef CUBELAB_CAMERA_FRAME_STRADDLE_CUBELABCAP_H
#define CUBELAB_CAMERA_FRAME_STRADDLE_CUBELABCAP_H

    #include "Spinnaker.h"
    #include "SpinGenApi/SpinnakerGenApi.h"

    #include <iostream>
    #include <fstream>
    #include <sstream>
    #include <cstdio>
    #include <cstdlib>
    #include <cstring>
    #include <cstdint>
    #include <thread>
    #include <chrono>

    #include <unistd.h>
    #include <sys/sysinfo.h>

    #include "camTempMon.h"

    #define MEM_FREE_MIN (1 << 29)

#endif //CUBELAB_CAMERA_FRAME_STRADDLE_CUBELABCAP_H

