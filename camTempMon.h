//
// Created by gnare on 1/31/24.
//

#ifndef CUBELAB_CAMERA_FRAME_STRADDLE_CAMTEMPMON_H
#define CUBELAB_CAMERA_FRAME_STRADDLE_CAMTEMPMON_H

    #include "cubelabCap.h"

    #include <csignal>
    #include <curlpp/cURLpp.hpp>
    #include <curlpp/Options.hpp>
    #include <curlpp/Easy.hpp>
    #include <curlpp/OptionBase.hpp>
    #include <curlpp/Infos.hpp>
    #include <curlpp/Info.hpp>

    #define API_HOST std::string("localhost:8080")

#endif //CUBELAB_CAMERA_FRAME_STRADDLE_CAMTEMPMON_H
