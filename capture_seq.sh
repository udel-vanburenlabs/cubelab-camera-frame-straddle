#!/bin/env bash

DEBUG=0

CAMERA=$1
NUMIMAGES=$2
FOOTAGEPATH=$3

dtstamp=$(date +%F_%H-%M-%S.%N)

set -e
set -o pipefail

function capture {
	if [ -z "${FOOTAGEPATH}" ]
	then
		FOOTAGEPATH="/mnt/ssd"
	fi
	rm -rf imageseq
	mkdir imageseq
	cd imageseq
	Acquisition -c ${CAMERA} ${NUMIMAGES} | tee acquisition.log
	ffmpeg -threads 12 -y -pattern_type glob -i "*.bmp" -vf format=gray -f rawvideo pipe: | ffmpeg -threads 12 -r 174.951 -y -f rawvideo -pixel_format bayer_rggb8 -video_size 1616x1240 -i pipe: -c:v rawvideo -pix_fmt gray "${FOOTAGEPATH}/capture-${dtstamp}.avi" | tee ffmpeg.log
	cd ..
	lbzip2 -9 -z "${FOOTAGEPATH}/capture-${dtstamp}.avi"
	echo ""
	echo "${FOOTAGEPATH}/capture-${dtstamp}.avi.bz2"
}

if [ $DEBUG == 1 ]
then
	time capture
else
	capture
fi
