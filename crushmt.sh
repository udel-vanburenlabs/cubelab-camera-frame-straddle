#!/bin/env bash

RETDIR=$(pwd)
TARGETDIR=$1
OUTDIR=${TARGETDIR}/crush

DEBUG=0

let "JOBC=0"
MAXJOBS=12

set -e

function runcrush {
	rm -r ${OUTDIR}
	mkdir ${OUTDIR}

	cd ${TARGETDIR}

	for PNGFILE in *.png
	do
		cd ${RETDIR}

		echo "${JOBC} pngcrush ${TARGETDIR}/${PNGFILE} to ${OUTDIR}/${PNGFILE}"
		pngcrush -new -c 0 -m 16 ${TARGETDIR}/${PNGFILE} ${OUTDIR}/${PNGFILE} &>> ${OUTDIR}/pngcrush.log &
    	let "JOBC=$(jobs -r -p | tr '\\n' ' ' | wc -w)"
		if [ $JOBC -ge $MAXJOBS ]
		then
			wait -n $(jobs -r -p | tr '\n' ' ')
		fi
	done
	wait $(jobs -p)

	dtstamp=$(date +%F_%H-%M-%S.%N)
	tar -cf ${TARGETDIR}/batch_crushed_${dtstamp}.tar ${TARGETDIR}/*.csv ${OUTDIR}/*.png
	lbzip2 -n 8 -z9 ${TARGETDIR}/batch_crushed_${dtstamp}.tar
	echo "done ${TARGETDIR}/batch_crushed_${dtstamp}.tar.bz2"
}

if [ $DEBUG == 1 ]
then
	time runcrush
else
	runcrush
fi
