# CubeLab Camera Frame Straddle

A collection of scripts and programs to interface with the FLIR cameras on the CubeLab project.

This project is designed to run on the Orange Pi 5 SBC, but can be tested on a normal Linux workstation.
It _WILL NOT WORK_ on any Windows system. In order to test on a Windows system, 
you will either need WSL2 or a Linux VM.

It is important to note that the FLIR cameras capture data with the Bayer RG8 pixel format. The raw data from the
cameras is not usable for particle image velocimetry (PIV) analysis, as the Bayer pattern must be debayered first.
Therefore, it is critical that FFMPEG is installed so that it can debayer the video data before it is stored. FFMPEG
converts the data to grayscale video to further reduce the size of the video data, as color information is not
necessary for the experiment.

The capture process will at times use a very large amount of memory and CPU time, so it is critical that the Orange Pi
does not carry out any other important or demanding tasks during the capture process.

If you are getting strange errors relating to the FLIR cameras not being available, this can be resolved by either
toggling the USB port (if using the programmable USB hub), or by unplugging and replugging the camera.

### Authors:

Galen Nare

---

### Requirements:

 - FLIR Spinnaker SDK
 - GNU Make
 - GCC and G++ compilers (the OpenMP extension _must_ be aviailable for the Acquisition programs)
 - FFMPEG (for the `capture_seq` and `capture_burst_seq` scripts)
 - Python 3 (for the Acquisition.py script for `rt_stream` script)
 - LBzip2 (for the `capture_seq` and `capture_burst_seq` scripts)
 - PNGCrush (**optional**, for the `crushmt` script) (https://pmt.sourceforge.io/pngcrush/)

### Installation:

 - Clone the repo using `git clone --recursive`
 - Ensure the FLIR Spinnaker SDK is installed on the system. This is required to interface with the FLIR cameras.
   - You can find the SDK here: https://www.flir.com/products/spinnaker-sdk/
   - On the Orange Pi 5, the SDK is installed in `/opt/spinnaker`, and the ARM64 variant is required.
   - The `SPINNAKER_GENTL_CI` environment variable may need to be set in order for compilation to work.
 - Run `make` in the project directory
 - Run `sudo make install` to install the compiled binaries and scripts to `/usr/bin`

### Usage:

##### `capture_seq`:

Calls the `Acquisition` program to capture a continous sequence of frames from the FLIR cameras. These frames are
passed through FFMPEG to consolidate them and adjust to the correct framerate. This then uses LBzip2 to compress the
raw video losslessly. The output is a `.avi.bz2` file which is stored on the onboard SSD by default (unless a 
different directory is specified).

**NOTE:** The `Acquisition` program stores all captured frames in memory temporarily, and allocates a contiguous memory
buffer to store them before capturing. It has an OOM failsafe that will prevent the capture from occurring if the
allocation fails. This is to prevent the system from crashing due to memory exhaustion. The capture will also
automatically abort if available memory drops below a certain threshold.

 - `capture_seq <camera_num> <frames_max> [output_cirectory]`

##### `capture_burst_seq`:

Same as the above, but captures a burst of frames separated by a configurable delay (in milliseconds),
instead of a continuous sequence. This can save storage space
as well as encoding time, as not all cases may be required to have continuous video.

All the notes and warnings from the `capture_seq` script apply here as well.

 - `capture_burst_seq <camera_num> <frames_per_burst> <number_of_bursts> <burst_delay_ms> [output_directory]`

##### `rtstream`:

This script is designed to capture a real-time stream of frames from the FLIR cameras. It uses the `Acquisition.py`
script to capture frames and pipe them into FFMPEG to create a real-time UDP video stream on port 48001 over HTTP. 
This can be connected to using a video player like VLC or OBS Studio.

Only one client may be connected at a time, and the stream will automatically stop if the client disconnects.

**_Make sure this script is not running when you try to use the capture_seq scripts as it locks the camera and may
freeze the camera driver._**

 - `rtstream`

##### Other scripts:

The other scripts in this project are no longer used, and may be used for testing or tinkering with the cameras,
but are not recommended for use in the final CubeLab project.

### License:

MIT

Feel free to use this code for whatever you want (credit appreciated but not required),
but if you claim you wrote it I will publicly shame you :)