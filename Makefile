################################################################################
# Acquisition Makefile
################################################################################
PROJECT_ROOT=/opt/spinnaker/src/Acquisition
OPT_INC = ${PROJECT_ROOT}/common/make/common_spin.mk
-include ${OPT_INC}

################################################################################
# Key paths and settings
################################################################################
CFLAGS += -std=c++11 -fopenmp
ifeq ($(wildcard ${OPT_INC}),)
CXX = g++ ${CFLAGS}
ODIR  = .obj/build${D}
SDIR  = .
MKDIR = mkdir -p
PLATFORM = $(shell uname)
ifeq ($(PLATFORM),Darwin)
OS = mac
endif
endif

OUTPUTNAME = Acquisition${D}
OUTDIR = ./bin/

################################################################################
# Dependencies
################################################################################
# Spinnaker deps
SPINNAKER_LIB = -L/opt/spinnaker/lib -lSpinnaker${D} ${SPIN_DEPS}

################################################################################
# Master inc/lib/obj/dep settings
################################################################################
_OBJ = Acquisition.o AcquisitionBurst.o usbreset.o camTempMon.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))
INC = -I${PROJECT_ROOT}/../../include
ifneq ($(OS),mac)
INC += -I/opt/spinnaker/include
LIB += -Wl,-Bdynamic ${SPINNAKER_LIB} -lSpinVideo
LIB += -Wl,-rpath-link=../../lib
else
INC += -I/usr/local/include/spinnaker
LIB += -rpath ../../lib/
LIB += ${SPINNAKER_LIB} -lSpinVideo
endif

################################################################################
# Rules/recipes
################################################################################

all: Acquisition AcquisitionBurst usbreset camTempMon

AcquisitionBurst: ${ODIR}/AcquisitionBurst.o
	${CXX} -o $@ $^ ${LIB}
	-mkdir ${OUTDIR}
	mv AcquisitionBurst ${OUTDIR}

camTempMon:  ${ODIR}/camTempMon.o
	${CXX} -o $@ $^ ${LIB} -lcurlpp -lcurl
	-mkdir ${OUTDIR}
	mv camTempMon ${OUTDIR}

usbreset: ${ODIR}/usbreset.o
	${CXX} -o $@ $<
	-mkdir ${OUTDIR}
	mv usbreset ${OUTDIR}

# Final binary
Acquisition: ${ODIR}/Acquisition.o
	${CXX} -o $@ $^ ${LIB}
	-mkdir ${OUTDIR}
	mv Acquisition ${OUTDIR}

# Intermediate object files
${OBJ}: ${ODIR}/%.o : ${SDIR}/%.cpp
	@${MKDIR} ${ODIR}
	${CXX} ${INC} -Wall -D LINUX -c $< -o $@

# Clean up intermediate objects
clean_obj:
	rm -f *.o
	rm -f ${OBJ}
	@echo "intermediate objects cleaned up!"

install: all
	cp ${OUTDIR}${OUTPUTNAME} /usr/bin
	cp ${OUTDIR}AcquisitionBurst /usr/bin
	cp ${OUTDIR}usbreset /usr/bin
	cp ${OUTDIR}camTempMon /usr/bin
	cp crushmt.sh /usr/bin/crushmt
	cp capture_seq.sh /usr/bin/capture_seq
	cp capture_burst_seq.sh /usr/bin/capture_burst_seq
	cp rtstream.sh /usr/bin/rtstream

# Clean up everything.
clean: clean_obj
	rm -rf .obj
	rm -rf ${OUTDIR}
	@echo "all cleaned up!"
