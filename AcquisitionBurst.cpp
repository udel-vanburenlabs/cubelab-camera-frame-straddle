#include "cubelabCap.h"

using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;
using namespace std;

bool dry_run = false;
bool doTerminate = false;

CameraPtr acqCamPtr;
SystemPtr flirSystem;
CameraList camList;
ImagePtr* cam_array;

void signal_handler(const int signal)
{
    std::cout << "Program recieved signal " << signal << std::endl;
    if (signal == SIGTERM) {
        std::cout << "Terminating, skipping the rest of the images and cleaing up." << std::endl;
        doTerminate = true;
    } else {
        std::cout << "Send SIGTERM if you really want to quit." << std::endl;
    }
}


inline bool file_exists(const std::string& name) {
    ifstream f(name.c_str());
    return f.good();
}

// This function acquires and saves 10 images from a device.
int AcquireImages(CameraPtr pCam, INodeMap& nodeMap, INodeMap& nodeMapTLDevice, unsigned int k_numImages, const unsigned int burstIdx)
{
    int result = 0;

    cout << endl << endl << "*** IMAGE ACQUISITION ***" << endl << endl;

    try
    {
        pCam->BeginAcquisition();

        cout << "Acquiring images for burst " << burstIdx << endl;

        gcstring deviceSerialNumber("");
        deviceSerialNumber = pCam->DeviceSerialNumber.GetValue();

        cout << "Allocating memory; if this fails you have requested too long of a capture." << endl;

        //Processor
        ImageProcessor processor;
        processor.SetColorProcessing(SPINNAKER_COLOR_PROCESSING_ALGORITHM_IPP);

        ImagePtr allocImage = pCam->GetNextImage(1000);
        ImagePtr convAllocImage = processor.Convert(allocImage, PixelFormat_BayerRG8);

        cam_array = new ImagePtr[k_numImages];

        // Preallocate memory, fail fast
        for (unsigned int i = 0; i < k_numImages; i++) {
            cam_array[i] = Image::Create();
            cam_array[i]->DeepCopy(convAllocImage);
        }

        int width = 0;
        int height = 0;
        long long frameTimeUs = 0;

        ofstream timestampsFile("AcquisitionBurst-" + deviceSerialNumber + "-timestamps.csv");

        //For Loop To Pull Images
        for (unsigned int imageCnt = 0; imageCnt < k_numImages && !doTerminate; imageCnt++)
        {
                // Retrieve next received image
                ImagePtr pResultImage = pCam->GetNextImage(1000);

                if (pResultImage->IsIncomplete())
                {
                    // Retrieve and print the image status description
                    cout << "Image incomplete: " << Image::GetImageStatusDescription(pResultImage->GetImageStatus())
                        << "..." << endl
                        << endl;
                }
                else
                {
                    #pragma omp parallel sections
                    {
                        #pragma omp section
                        {
                            // Print image information
                            width = pResultImage->GetWidth();
                            height = pResultImage->GetHeight();
                            cout << "Grabbed image " << imageCnt << ", width = " << width << ", height = " << height << endl;
                            frameTimeUs = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now().time_since_epoch()).count();
                            timestampsFile <<  burstIdx << "," << imageCnt <<  "," <<  frameTimeUs << "," << pCam->AcquisitionResultingFrameRate.GetValue() << endl;
                        }
                        #pragma omp section
                        {
                            // Convert image to something.
                            // Doing this returns a pointer that isnt linked to the camera,
                            // this is necessary as we are encoding after recording all of the images.
                            ImagePtr convertedImage = processor.Convert(pResultImage, PixelFormat_BayerRG8);

                            //Throw it in the array
                            cam_array[imageCnt]->DeepCopy(convertedImage);
                        }
                    }
                }

                ImagePtr image = cam_array[imageCnt];

                //Release OG Pictre from the Cam
                pResultImage->Release();
        }

        // End acquisition
        pCam->EndAcquisition();

        //For loop for saving images
        cout << "This will save images not in order. Don't worry this is supposed to happen." << endl;
        if (!dry_run) {
            #pragma omp parallel for ordered schedule(dynamic)
            for (unsigned int imageCnt = 0; imageCnt < k_numImages; imageCnt++) {
                if (doTerminate) {
                    continue;
                }

                ImagePtr image = cam_array[imageCnt];

                // Create a unique filename
                ostringstream filename;
                    filename << "AcquisitionBurst-";
                if (!deviceSerialNumber.empty())
                {
                    filename << deviceSerialNumber.c_str() << "-burst_" << burstIdx << "-";
                }
                filename << string(4 - to_string(imageCnt).size(), '0').append(to_string(imageCnt)) << ".bmp";
            
                // Save image
            
                // *** NOTES ***
                // The standard practice of the examples is to use device
                // serial numbers to keep images of one device from
                // overwriting those of another.
            
                image->Save(filename.str().c_str());
                cout << "Image saved at " << filename.str() << endl;
                // video.Append(image);
            }
        }

    delete[] cam_array;

    }
    catch (Spinnaker::Exception& e)
    {
        cout << "Error: " << e.what() << endl;
        return -1;
    }

    return result;
}

// Formats camera to appropriate things
int FormatCam(CameraPtr pCam){

    cout << endl << endl << "*** Camera Format ***" << endl << endl;

    int result = 0;
    PixelFormatEnums TargetFormat = PixelFormat_BayerRG8;

    //Set Pixel Format
    pCam->PixelFormat.SetValue(TargetFormat);
   
    //Set FPS Things
    pCam->ExposureAuto.SetValue(ExposureAuto_Off);
    pCam->ExposureTime.SetValue(870);
    pCam->AcquisitionFrameRateEnable.SetValue(true);
    pCam->AcquisitionFrameRate.SetValue(175);
    pCam->AcquisitionMode.SetValue(AcquisitionMode_Continuous);
    cout << pCam->AcquisitionFrameRate.GetValue();

    //Other

    return result;
}

// This function acts as the body of the example; please see NodeMapInfo example
// for more in-depth comments on setting up cameras.
int RunSingleCamera(CameraPtr pCam, const unsigned int numImages, const unsigned long delayMs, const unsigned int numBursts)
{
    int result;

    try
    {
        // Retrieve TL device nodemap and print device information
        INodeMap& nodeMapTLDevice = pCam->GetTLDeviceNodeMap();

        // Initialize camera
        pCam->Init();

        // Retrieve GenICam nodemap
        INodeMap& nodeMap = pCam->GetNodeMap();

        //We do some formatting
        result = FormatCam(pCam);

        for (unsigned int i = 0; i < numBursts; i++) {
            // Acquire images
            result = result | AcquireImages(pCam, nodeMap, nodeMapTLDevice, numImages, i);
            cout << "sleeping for " << delayMs << " ms." << endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(delayMs));
        }

        // Deinitialize camera
        pCam->DeInit();
    }
    catch (Spinnaker::Exception& e)
    {
        cout << "Error: " << e.what() << endl;
        result = -1;
    }

    return result;
}

// Example entry point; please see Enumeration example for more in-depth
// comments on preparing and cleaning up the system.
int main(int argc, char** argv)
{
    if (argc < 4) {
        cerr << "Please provide a number of frames/bursts to record. (usage: 'AcquisitionBurst [-c <cam_index>] [-d] <bursts> <burstDelayMs> <num>')" << endl;
        return 1;
    }

    unsigned int camera = 0;

    for (int i = 1; i < argc ; i++) {
        if (strncmp(argv[i], "-d", 4) == 0) {
            dry_run = true;
        }

        if (strncmp(argv[i], "-c", 4) == 0) {
            camera = atoi(argv[i + 1]);
        }
    }

    const unsigned int numBursts = atoi(argv[argc - 3]);
    const unsigned long burstDelayMs = atol(argv[argc - 2]);
    const unsigned int numImages = atoi(argv[argc - 1]);

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    // Since this application saves images in the current folder
    // we must ensure that we have permission to write to this folder.
    // If we do not have permission, fail right away.
    FILE* tempFile = fopen("test.txt", "w+");
    if (tempFile == nullptr)
    {
        cout << "Failed to create file in current folder.  Please check "
            "permissions."
            << endl;
        return -1;
    }
    fclose(tempFile);
    remove("test.txt");

    // Print application build information
    cout << "Application build date: " << __DATE__ << " " << __TIME__ << endl << endl;

    // Retrieve singleton reference to flirSystem object
    flirSystem = System::GetInstance();

    // Print out current library version
    const LibraryVersion spinnakerLibraryVersion = flirSystem->GetLibraryVersion();
    cout << "Spinnaker library version: " << spinnakerLibraryVersion.major << "." << spinnakerLibraryVersion.minor
        << "." << spinnakerLibraryVersion.type << "." << spinnakerLibraryVersion.build << endl
        << endl;

    // Retrieve list of cameras from the flirSystem
    camList = flirSystem->GetCameras();

    int numCameras = camList.GetSize();

    cout << "Number of cameras detected: " << numCameras << endl << endl;

    // Finish if there are no cameras
    if (numCameras == 0)
    {
        // Clear camera list before releasing flirSystem
        camList.Clear();

        // Release flirSystem
        flirSystem->ReleaseInstance();

        cout << "Not enough cameras!" << endl;

        return -1;
    }

    // Create shared pointer to camera
    //
    // *** NOTES ***
    // The CameraPtr object is a shared pointer, and will generally clean itself
    // up upon exiting its scope. However, if a shared pointer is created in the
    // same scope that a flirSystem object is explicitly released (i.e. this scope),
    // the reference to the shared point must be broken manually.
    //
    // *** LATER ***
    // Shared pointers can be terminated manually by assigning them to nullptr.
    // This keeps releasing the flirSystem from throwing an exception.
    //
    acqCamPtr = nullptr;

    int result = 0;

    if (file_exists("flircamera.lock")) {
        cout << "Found camera lock file, attempting to remove" << endl;
        // Select camera
        acqCamPtr = camList.GetByIndex(camera);

        // Retrieve TL device nodemap and print device information
        INodeMap& nodeMapTLDevice = acqCamPtr->GetTLDeviceNodeMap();

        // Initialize camera
        acqCamPtr->Init();

        // Retrieve GenICam nodemap
        INodeMap& nodeMap = acqCamPtr->GetNodeMap();

        // Access the DeviceReset command
        Spinnaker::GenApi::CCommandPtr pDeviceReset = nodeMap.GetNode("DeviceReset");

        // Check if the DeviceReset command is available and executable
        if (Spinnaker::GenApi::IsAvailable(pDeviceReset) && Spinnaker::GenApi::IsWritable(pDeviceReset)) {
            cout << "Resetting camera and sleeping for 5 seconds" << endl;
            // Execute the DeviceReset command
            pDeviceReset->Execute();

            this_thread::sleep_for(chrono::seconds(5));

            remove("flircamera.lock");
            cout << "Camera successfully reset" << endl;
        }

        // Retrieve list of cameras from the flirSystem
        camList = flirSystem->GetCameras();

        numCameras = camList.GetSize();

        cout << "Number of cameras detected: " << numCameras << endl << endl;

        // Finish if there are no cameras
        if (numCameras == 0)
        {
            // Clear camera list before releasing flirSystem
            camList.Clear();

            // Release flirSystem
            flirSystem->ReleaseInstance();

            cout << "Not enough cameras!" << endl;

            return 254;
        }
    }

    ofstream lockfile ("flircamera.lock");
    lockfile << "1" << endl;
    lockfile.close();

    // Select camera
    acqCamPtr = camList.GetByIndex(camera);

    cout << endl << "Running capture for camera " << camera << "..." << endl;

    // Run
    result = result | RunSingleCamera(acqCamPtr, numImages, burstDelayMs, numBursts);

    cout << "Camera " << camera << " capture complete..." << endl << endl;

    //
    // Release reference to the camera
    //
    // *** NOTES ***
    // Had the CameraPtr object been created within the for-loop, it would not
    // be necessary to manually break the reference because the shared pointer
    // would have automatically cleaned itself up upon exiting the loop.
    //
    acqCamPtr = nullptr;

    // Clear camera list before releasing flirSystem
    camList.Clear();

    // Release flirSystem
    flirSystem->ReleaseInstance();

    remove("flircamera.lock");

    return result;
}
