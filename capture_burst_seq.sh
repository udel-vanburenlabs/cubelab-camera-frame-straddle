#!/bin/env bash

DEBUG=0

CAMERA=$1
NUMIMAGES=$2
NBURST=$3
DELAY=$4
FOOTAGEPATH=$5
MAXJOBS=12

dtstamp=$(date +%F_%H-%M-%S.%N)

set -e
set -o pipefail

function capture {
	if [ -z "${FOOTAGEPATH}" ]
	then
		FOOTAGEPATH="/mnt/ssd"
	fi
	rm -rf burstseq
	mkdir burstseq
	cd burstseq
	AcquisitionBurst -c ${CAMERA} ${NBURST} ${DELAY} ${NUMIMAGES} | tee acquisition.log
	for i in $(ls *.bmp)
	do
		ffmpeg -hide_banner -y -i $i -vf format=gray -f rawvideo pipe: | ffmpeg -hide_banner -y -f rawvideo -pixel_format bayer_rggb8 -video_size 1616x1240 -i pipe: $i.png && rm $i &
	done
	cd ..
	let "JOBC=$(jobs -r -p | tr '\\n' ' ' | wc -w)"
	if [ $JOBC -ge $MAXJOBS ]
	then
		wait -n $(jobs -r -p | tr '\n' ' ')
	fi
	tar -czvf ${FOOTAGEPATH}/burst-capture-${dtstamp}.tar burstseq
	lbzip2 -9 -z ${FOOTAGEPATH}/burst-capture-${dtstamp}.tar
	echo ""
	echo ${FOOTAGEPATH}/burst-capture-${dtstamp}.tar.bz2
}

if [ $DEBUG == 1 ]
then
	time capture
else
	capture
fi
