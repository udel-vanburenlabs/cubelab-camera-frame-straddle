//
// Created by gnare on 1/31/24.
//

#include "camTempMon.h"

Spinnaker::SystemPtr flirSystem = Spinnaker::System::GetInstance();
Spinnaker::CameraList camList;
Spinnaker::CameraPtr acqCamPtr;

void signal_handler(const int signal)
{
    std::cout << "Program recieved signal " << signal << std::endl;
    std::cout << "Shutting down." << std::endl;
    acqCamPtr->DeInit();
    acqCamPtr = nullptr;
    camList.Clear();
    flirSystem->ReleaseInstance();
    curlpp::terminate();
    std::exit(signal);
}

int main(int argc, char** argv) {
    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    unsigned int camera = 0;

    for (int i = 1; i < argc ; i++) {
        if (strncmp(argv[i], "-c", 4) == 0) {
            camera = atoi(argv[i + 1]);
        }
    }

    std::string host = API_HOST;
    cURLpp::initialize();

    if (getenv("API_HOST") != nullptr && !std::string(getenv("API_HOST")).empty()) {
        host = std::string(getenv("API_HOST"));
    }

    // Retrieve list of cameras from the flirSystem
    camList = flirSystem->GetCameras();

    unsigned int numCameras = camList.GetSize();

    std::cout << "Number of cameras detected: " << numCameras << std::endl << std::endl;

    // Finish if there are no cameras
    if (numCameras == 0)
    {
        // Clear camera list before releasing flirSystem
        camList.Clear();

        // Release flirSystem
        flirSystem->ReleaseInstance();

        std::cout << "Not enough cameras!" << std::endl;

        return 254;
    }

    acqCamPtr = camList.GetByIndex(camera);

    acqCamPtr->Init();

    Spinnaker::GenApi::CFloatPtr tempPtr = acqCamPtr->GetNodeMap().GetNode("DeviceTemperature");
#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
    while(true) {

        try {
            double camTempC = tempPtr->GetValue();
            std::cout << "Camera " << camera << " temp: " << camTempC << " C" << std::endl;

            try {
                std::ostringstream bodyStream;

                std::time_t timestamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
                std::tm* tsTm = std::localtime(&timestamp);

                char tsStr[255];
                std::strftime(tsStr, 32, "%Y-%m-%d %H:%M:%S.000000", tsTm);

                bodyStream << "{" << std::endl;
                bodyStream << R"(    "id": 0,)" << std::endl;
                bodyStream << R"(    "cameraId": )" << camera << "," << std::endl;
                bodyStream << R"(    "timestamp": ")" << tsStr << "\"," << std::endl;
                bodyStream << R"(    "temperature": )" << camTempC << std::endl;
                bodyStream << "}" << std::endl;

                std::string body = bodyStream.str();
//                std::cout << body << std::endl;

                std::list<std::string> header;
                header.push_back("Content-Type: application/json");

                curlpp::Cleanup clean;
                curlpp::Easy req;
                req.setOpt(curlpp::options::Url("http://" + host + "/api/camera-temp/"));
                req.setOpt(curlpp::options::HttpHeader(header));
                req.setOpt(curlpp::options::PostFields(body));
                req.setOpt(curlpp::options::PostFieldSize(body.length()));

                std::ostringstream response;
                req.setOpt(curlpp::options::WriteStream(&response));

                req.perform();

                unsigned long code = curlpp::infos::ResponseCode::get(req);

                if (code < 200 || code >= 300) {
                    std::cerr << "POST /api/camera-temp/ returned code " << code << std::endl;
                }
            } catch (curlpp::RuntimeError& ce) {
                std::cerr << "Failed to POST camera temp to API:" << std::endl;
                std::cerr << ce.what() << std::endl;
            }
        } catch (Spinnaker::Exception& se) {
            std::cerr << "Failed to get device temperature:" << std::endl;
            std::cerr << se.GetErrorMessage() << std::endl;
        }

        std::this_thread::sleep_for(std::chrono::seconds(5));
    }
#pragma clang diagnostic pop
}
