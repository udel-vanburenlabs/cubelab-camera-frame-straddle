#!/bin/env bash
ip=$(ifconfig eth0 | grep "inet " | python -c "print(input().split()[1])")
./Acquisition.py | ffmpeg -pix_fmt gray -f rawvideo -video_size 1616x1240 -r 55 -i pipe: -vsync 2 -r 55 -b:v 8000k -maxrate 8000k -minrate 8000k -bufsize 8000k -preset ultrafast -f mpegts -c:v libx264 -c:a none -listen 1 http://$ip:48001
